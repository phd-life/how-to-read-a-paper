# how to read a paper

instuctions on how to read a paper and template for note-taking

## reference

[How to Read a Paper](http://www.sigcomm.org/sites/default/files/ccr/papers/2007/July/1273445-1273458.pdf)

[How to Read a Technical Paper](https://www.cs.jhu.edu/~jason/advice/how-to-read-a-paper.html)
